FROM centos

RUN yum update -y && yum install -y iproute && yum clean all

RUN echo -e '[sensu] \nname=sensu \nbaseurl=http://repositories.sensuapp.org/yum/$basearch/ \ngpgcheck=0 \nenabled=1 \n' > /etc/yum.repos.d/sensu.repo

RUN yum install -y sensu && yum clean all

ENV PATH=$PATH:/opt/sensu/embedded/bin

RUN sensu-install -p disk-checks:1.0.2

RUN sensu-install -p process-checks:0.0.6

RUN sensu-install -p cpu-checks:0.0.3

COPY ./files /

CMD ["/usr/local/bin/exec.sh","sensu-server"]
